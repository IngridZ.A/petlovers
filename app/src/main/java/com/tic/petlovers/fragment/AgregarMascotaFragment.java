package com.tic.petlovers.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.PermissionChecker;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.tic.petlovers.MenuHomeActivity;
import com.tic.petlovers.R;
import com.tic.petlovers.model.Mascota;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

public class AgregarMascotaFragment extends Fragment {
    private DatabaseReference mDatabase;
    private StorageReference mStorage;
    private FirebaseAuth mAuth;
    private FusedLocationProviderClient providerClient;
    private EditText edNombreRegistroMascotas,edRazaRegistroMascotas, edTelefonoRegistroMascotas, edDireccionMascotas, edEdadRegistroMascotas;
    private Spinner spinnerCategoriaMascotas;
    private RadioButton rbMachoMascotas, rbHembraMascotas;
    private CheckBox cbDespMascotas, cbTripleMascotas, cbOtrasVMascotas, cbRabiaMascotas;
    private Button btnGuardarMascotas;
    private ImageView imgRegistroMascotas,btnUpRegistroMascota,btnGaleriaRegistroMascota;
    private String saveUri;
    private final int CODE_IMAGE_STORE = 1;
    private final int CODE_MEDIA_STORE = 2;
    public static final int RESULT_OK = -1;
    private final int CODE_PERMISSION_GPS = 3;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_agregar_mascota, container, false);
        permisosGps();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mStorage = FirebaseStorage.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();

        providerClient = LocationServices.getFusedLocationProviderClient(getActivity());
        imgRegistroMascotas = view.findViewById(R.id.imgRegistroMascotas);
        btnUpRegistroMascota = view.findViewById(R.id.btnUpRegistroMascota);
        btnGaleriaRegistroMascota = view.findViewById(R.id.btnGaleriaRegistroMascota);
        edNombreRegistroMascotas = view.findViewById(R.id.edNombreRegistroMascotas);
        edEdadRegistroMascotas = view.findViewById(R.id.edEdadRegistroMascotas);
        edRazaRegistroMascotas = view.findViewById(R.id.edRazaRegistroMascotas);
        spinnerCategoriaMascotas = view.findViewById(R.id.spinnerCatMascotasMascota);
        rbMachoMascotas = view.findViewById(R.id.rbMachoMascotas);
        rbHembraMascotas = view.findViewById(R.id.rbHembraMascotas);
        edTelefonoRegistroMascotas = view.findViewById(R.id.edTelefonoRegistroMascotas);
        edDireccionMascotas = view.findViewById(R.id.edDireccionMascotas);
        cbDespMascotas = view.findViewById(R.id.cbDespMascotas);
        cbTripleMascotas = view.findViewById(R.id.cbTripleMascotas);
        cbOtrasVMascotas = view.findViewById(R.id.cbOtrasVMascotas);
        cbRabiaMascotas = view.findViewById(R.id.cbRabiaMascotas);
        btnGuardarMascotas = view.findViewById(R.id.btnGuardarMascotas);
        imgRegistroMascotas.setImageResource(R.drawable.selfie);
        boolean permisoCamara = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA)== PermissionChecker.PERMISSION_GRANTED;
        boolean permisoGaleria = ContextCompat.checkSelfPermission(getContext(),Manifest.permission.READ_EXTERNAL_STORAGE)== PermissionChecker.PERMISSION_GRANTED;
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),R.array.spinner_tipo_mascota, android.R.layout.simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCategoriaMascotas.setAdapter(adapter);

        btnUpRegistroMascota.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (permisoCamara){

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, CODE_IMAGE_STORE);
                } else {
                    ActivityCompat.requestPermissions(getActivity(),new String[] {Manifest.permission.CAMERA},2);
                }
            }
        });

        btnGaleriaRegistroMascota.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (permisoGaleria){

                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, CODE_MEDIA_STORE);
                } else {
                    ActivityCompat.requestPermissions(getActivity(),new String[] {Manifest.permission.READ_EXTERNAL_STORAGE},1);
                }
            }
        });

        btnGuardarMascotas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                agregarMascotaDB();
            }
        });
        return view;

    }

    @SuppressLint("MissingPermission")
    private void agregarMascotaDB() {
        if (permisosGps()){
            providerClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    if (location != null){
                        FirebaseUser currentUser = mAuth.getCurrentUser();
                        String id = currentUser.getUid();
                        String nombreM = edNombreRegistroMascotas.getText().toString();
                        String razaM = edRazaRegistroMascotas.getText().toString();
                        String telefonoM = edTelefonoRegistroMascotas.getText().toString();
                        String direccionM = edDireccionMascotas.getText().toString();
                        String edadM = edEdadRegistroMascotas.getText().toString()+" meses";
                        String categoriaM = spinnerCategoriaMascotas.getSelectedItem().toString();
                        Double longitud = location.getLongitude();
                        Double latitud = location.getLatitude();
                        ArrayList<String> vacunas = new ArrayList<>();
                        if (cbRabiaMascotas.isChecked()) vacunas.add("Rabia");
                        if (cbOtrasVMascotas.isChecked()) vacunas.add("Vacunas complementarias");
                        if (cbTripleMascotas.isChecked()) vacunas.add("Triple");
                        if (cbDespMascotas.isChecked()) vacunas.add("Desparacitación");


                        Mascota animal = new Mascota(nombreM, razaM, telefonoM, direccionM, edadM, saveUri, id, vacunas, longitud, latitud);
                        if(rbMachoMascotas.isChecked()){
                            mDatabase.child("Mascotas").child(categoriaM).child("Macho").push().setValue(animal);

                        } else {
                            mDatabase.child("Mascotas").child(categoriaM).child("Hembra").push().setValue(animal);
                        }
                    }
                }

            });
            Toast.makeText(getContext(), "Se registro la mascota correctamente", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getContext(), "No se pudo registrar la mascota. Verifique los permisos de GPS", Toast.LENGTH_LONG).show();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK && data != null){
            String nameImage = randomNameImage();
            UploadTask uploadTask;
            switch(requestCode){
                //Cámara
                case CODE_IMAGE_STORE:
                    Bundle extra = data.getExtras();
                    Bitmap bitmap =  (Bitmap) extra.get("data");
                    imgRegistroMascotas.setImageBitmap(bitmap);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    byte[] dataImage = baos.toByteArray();
                    uploadTask = mStorage.child("Imágenes_mascotas_en_adopción").child(nameImage).putBytes(dataImage);
                    uriImageStorage (uploadTask, nameImage);
                    break;

                //Galeria
                case CODE_MEDIA_STORE:
                    Uri uriImg = data.getData();
                    imgRegistroMascotas.setImageURI(uriImg);
                    uploadTask = mStorage.child("Imágenes_mascotas_en_adopción").child(nameImage).putFile(uriImg);
                    uriImageStorage (uploadTask, nameImage);
                    break;
            }
        }
    }

    private void uriImageStorage(UploadTask uploadTask, String nameImage) {
        uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()){
                    throw task.getException();
                }
                return mStorage.child("Imágenes_mascotas_en_adopción").child(nameImage).getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()){
                    Uri downloadUri = task.getResult();
                    saveUri = downloadUri.toString();
                } else {
                    Log.w("FailUploadImage", "putBytes(dataImage):failure", uploadTask.getException());
                    Toast.makeText(getContext(), "No fue posible cargar la imágen al servidor", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    //método para generar el nombre que será asignado a la img en la BD
    private String randomNameImage() {
        FirebaseUser currentUser = mAuth.getCurrentUser();
        String id = currentUser.getUid();
        Date date = new Date();
        Random random = new Random();
        int numRandom = random.nextInt(999999);
        return "IMG"+id+"-"+date+"-"+numRandom+".jpeg";
    }

    private boolean permisosGps(){
        Boolean permisosAccesFine = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PermissionChecker.PERMISSION_GRANTED;
        Boolean permisosCoarseAcces = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PermissionChecker.PERMISSION_GRANTED;
        if (permisosAccesFine || permisosCoarseAcces){
            return true;

        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{ Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION}, CODE_PERMISSION_GPS);
            return false;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}

