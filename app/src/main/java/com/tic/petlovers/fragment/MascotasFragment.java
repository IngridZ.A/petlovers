package com.tic.petlovers.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.Spinner;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.tic.petlovers.R;
import com.tic.petlovers.adapter.MascotasAdapter;
import com.tic.petlovers.model.Mascota;

import java.util.LinkedList;


public class MascotasFragment extends Fragment {
    private DatabaseReference mDatabase;
    private RadioButton rbTipoMascota, rbGeneroMascota, rbTodosMascota;
    private Spinner spinnerCatMascotasMascota, spinnerGenMascotasMascota;
    private ImageButton btnBuscarMascota;
    private RecyclerView recyclerMascotaMain;
    private MascotasAdapter mascotasAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_mascotas, container, false);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        rbTipoMascota = view.findViewById(R.id.rbTipoMascota);
        rbGeneroMascota = view.findViewById(R.id.rbGeneroMascota);
        rbTodosMascota = view.findViewById(R.id.rbTodosMascota);
        spinnerCatMascotasMascota = view.findViewById(R.id.spinnerCatMascotasMascota);
        spinnerGenMascotasMascota = view.findViewById(R.id.spinnerGenMascotasMascota);
        btnBuscarMascota = view.findViewById(R.id.btnBuscarMascota);
        recyclerMascotaMain = view.findViewById(R.id.recyclerMascotaMain);

        ArrayAdapter<CharSequence> tipoM = ArrayAdapter.createFromResource(getContext(),R.array.spinner_tipo_mascota, android.R.layout.simple_spinner_dropdown_item);
        tipoM.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCatMascotasMascota.setAdapter(tipoM);

        ArrayAdapter<CharSequence> generoM = ArrayAdapter.createFromResource(getContext(),R.array.spinner_genero, android.R.layout.simple_spinner_dropdown_item);
        generoM.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerGenMascotasMascota.setAdapter(generoM);


        rbTipoMascota.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                spinnerCatMascotasMascota.setVisibility(isChecked ? View.VISIBLE : View.GONE);
                btnBuscarMascota.setVisibility(View.VISIBLE);
            }
        });

        rbGeneroMascota.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                spinnerGenMascotasMascota.setVisibility(isChecked ? View.VISIBLE : View.GONE);
                btnBuscarMascota.setVisibility(View.VISIBLE);
            }
        });

        if (rbTodosMascota.isChecked()){
            btnBuscarMascota.setVisibility(View.GONE);
        }

        btnBuscarMascota.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {cardViews();}
        });
        return view;
    }

    private void cardViews(){
        LinkedList<Mascota> mascotaList = new LinkedList<>();
        mascotaList.clear();
        String tipoMascota = spinnerCatMascotasMascota.getSelectedItem().toString();
        DatabaseReference reference = null;
        String generoMascota = spinnerGenMascotasMascota.getSelectedItem().toString();
        if (generoMascota != null){
            if (generoMascota.equals("Macho")){
                reference = mDatabase.child("Mascotas").child(tipoMascota).child("Macho");
            } else if (generoMascota.equals("Hembra")){
                reference = mDatabase.child("Mascotas").child(tipoMascota).child("Hembra");
            } else {
                reference = null;
            }
        }

        reference.get().addOnCompleteListener(new OnCompleteListener<DataSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DataSnapshot> task) {
                if (!task.isSuccessful()) {
                    Log.e("firebase", "Error getting data", task.getException());
                }
                else {
                    if(task.getResult().getValue()==null){
                        mascotasAdapter = new MascotasAdapter(mascotaList);
                        recyclerMascotaMain.setLayoutManager(new LinearLayoutManager(getContext()));
                        recyclerMascotaMain.setAdapter(mascotasAdapter);
                    }
                }
            }
        });

        reference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                Mascota mascota = snapshot.getValue(Mascota.class);
                mascotaList.add(mascota);
                mascotasAdapter = new MascotasAdapter(mascotaList);
                recyclerMascotaMain.setLayoutManager(new LinearLayoutManager(getContext()));
                recyclerMascotaMain.setAdapter(mascotasAdapter);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                System.out.println("onChildChanged");
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {
                System.out.println("onChildRemoved");
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                System.out.println("onChildMoved");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                System.out.println("onCancelled");
            }
        });

    }
}



