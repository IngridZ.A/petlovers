package com.tic.petlovers.controller;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;


import com.tic.petlovers.fragment.AgregarMascotaFragment;
import com.tic.petlovers.fragment.FavoritosFragment;
import com.tic.petlovers.fragment.MascotasFragment;

public class PagerController extends FragmentPagerAdapter {
    private int numOfTabs;

    public PagerController(@NonNull FragmentManager fm, int numOfTabs) {
        super(fm, numOfTabs);
        this.numOfTabs = numOfTabs;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new MascotasFragment();

            case 1:
                return new AgregarMascotaFragment();
            case 2:
                return new FavoritosFragment();
            default:
                return  null;

         }

           }

    @Override
    public int getCount() {
        return numOfTabs;
    }
}
