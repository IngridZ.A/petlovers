package com.tic.petlovers.adapter;

import static com.squareup.picasso.Picasso.*;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.tic.petlovers.R;
//import com.tic.petlovers.VisualizarMascotaActivity;
import com.tic.petlovers.VisualizarMascotaActivity;
import com.tic.petlovers.model.Mascota;

import java.util.List;

public class MascotasAdapter extends RecyclerView.Adapter<MascotasAdapter.ViewHolder> {

    static List<Mascota> mascotaList;


    public MascotasAdapter(List<Mascota> mascotaList) {
        this.mascotaList = mascotaList;
    }

    @NonNull
    @Override
    public MascotasAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_mascota, parent, false);
        return new ViewHolder(itemView);
    }
    @SuppressLint("StaticFielLeak")
    @Override
    public void onBindViewHolder(@NonNull MascotasAdapter.ViewHolder holder, int position) {
        holder.txtNombreMascotaCard.setText(mascotaList.get(position).getNombre());
        holder.txtEdadMascotaCard.setText("Edad: " + mascotaList.get(position).getEdad());
        holder.txtRazaMascotaCard.setText("Raza: " + mascotaList.get(position).getRaza());
        holder.txtGeneroMasctaCard.setText("Genero: " + mascotaList.get(position).getGenero());


        Picasso.get()
                .load(mascotaList.get(position).getUrlImagen())
                .resize(100,100)
                .centerCrop()
                .placeholder(R.drawable.logo)
                .error(R.drawable.cam)
                .into(holder.imgMascotasCard);


        holder.position=holder.getAdapterPosition();
    }

    @Override
    public int getItemCount() {
        return mascotaList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtNombreMascotaCard, txtEdadMascotaCard, txtRazaMascotaCard, txtGeneroMasctaCard;
        private ImageView imgMascotasCard,icFavoriteCardMascota;
        private int position;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtNombreMascotaCard = itemView.findViewById(R.id.txtNombreMascotaCard);
            txtEdadMascotaCard = itemView.findViewById(R.id.txtEdadMascotaCard);
            txtRazaMascotaCard = itemView.findViewById(R.id.txtRazaMascotaCard);
            txtGeneroMasctaCard = itemView.findViewById(R.id.txtGeneroMasctaCard);
            imgMascotasCard = itemView.findViewById(R.id.imgMascotasCard);
            icFavoriteCardMascota = itemView.findViewById(R.id.icFavoriteCardMascota);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(itemView.getContext(), VisualizarMascotaActivity.class);
                    System.out.println("Error");
                    intent.putExtra("urlImagen",mascotaList.get(position).getUrlImagen());
                    intent.putExtra("nombre",mascotaList.get(position).getNombre());
                    intent.putExtra("raza",mascotaList.get(position).getRaza());
                    intent.putExtra("edad",mascotaList.get(position).getEdad());
                    intent.putExtra("idUsuario",mascotaList.get(position).getIdUsuario());
                    intent.putExtra("telefono",mascotaList.get(position).getTelefono());
                    intent.putExtra("direccion",mascotaList.get(position).getDireccion());
                    intent.putExtra("latitud",mascotaList.get(position).getLatitud());
                    intent.putExtra("longitud",mascotaList.get(position).getLongitud());
                    itemView.getContext().startActivity(intent);
                }
            });

            //Toda la lógica para el boton de favoritos
            icFavoriteCardMascota.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("ResourceType")
                @Override
                public void onClick(View v) {
                    if (mascotaList.get(position).isFavorito()){
                        icFavoriteCardMascota.setImageResource(R.drawable.ic_baseline_favorite_border_vacio);
                        mascotaList.get(position).setFavorito(false);
                    }
                    else {
                        icFavoriteCardMascota.setImageResource(R.drawable.ic_baseline_favorite_lleno);
                        mascotaList.get(position).setFavorito(true);
                    }
                }
            });
        }
    }
}
