package com.tic.petlovers;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.tic.petlovers.model.Usuario;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class RegistroActivity extends AppCompatActivity {

    private EditText edUsuarioRegistro, edContrasenaRegistro, edConfirmarContrasenaRegistro, edCorreoRegistro, edFechaNacimientoRegistro;
    private ImageButton imgBtnCalendarioRegistro;
    private ConstraintLayout layoutRegistro;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        edUsuarioRegistro = findViewById(R.id.edUsuarioRegistro);
        edContrasenaRegistro = findViewById(R.id.edContrasenaRegistro);
        edConfirmarContrasenaRegistro = findViewById(R.id.edConfirmarContrasenaRegistro);
        edCorreoRegistro = findViewById(R.id.edCorreoRegistro);
        edFechaNacimientoRegistro = findViewById(R.id.edFechaNacimientoRegistro);
        imgBtnCalendarioRegistro = findViewById(R.id.imgBtnCalendarioRegistro);
        layoutRegistro = findViewById(R.id.layoutRegistro);


        imgBtnCalendarioRegistro.setOnClickListener(new View.OnClickListener() {
            Calendar calendar = Calendar.getInstance();
            int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
            int month = calendar.get(Calendar.MONTH);
            int year = calendar.get(Calendar.YEAR);

            @Override
            public void onClick(View view) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(RegistroActivity.this, R.style.DialogTheme,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
                                calendar.set(year, month, dayOfMonth);
                                Date date = calendar.getTime();
                                String fechaFormateada = new SimpleDateFormat("dd/MM/yyyy").format(date);
                                edFechaNacimientoRegistro.setText(fechaFormateada);
                            }
                        }, year, month, dayOfMonth);
                datePickerDialog.show();
            }
        });
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
    }

    public void registrarUsuario(View view) {
        String nombre = edUsuarioRegistro.getText().toString();
        String contraseña = edContrasenaRegistro.getText().toString();
        String confirmarcontraseña = edConfirmarContrasenaRegistro.getText().toString();
        String correo = edCorreoRegistro.getText().toString();
        String fechanacimiento = edFechaNacimientoRegistro.getText().toString();

        if (camposVacios(nombre, contraseña, confirmarcontraseña, correo, fechanacimiento))
            if (contraseña.equals(confirmarcontraseña)) {
                mAuth.createUserWithEmailAndPassword(correo, contraseña)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                FirebaseUser currentUser = mAuth.getCurrentUser();
                                String id = currentUser.getUid();
                                Usuario usuario = new Usuario(nombre, correo, fechanacimiento);
                                mDatabase.child("Usuarios").child(id).setValue(usuario);
                                Toast.makeText(RegistroActivity.this, "Registro exitoso", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(RegistroActivity.this,MenuHomeActivity.class);
                                startActivity(intent);
                                finish();

                            } else {
                                Log.w("FailRegister", "createUserWithEmailAndPassword:failure", task.getException());
                                Toast.makeText(RegistroActivity.this, "No fue posible registrar el usuario", Toast.LENGTH_LONG).show();
                            }
                        }
                    });

        } else {
            edContrasenaRegistro.setError("Los campos no coinciden");
            edConfirmarContrasenaRegistro.setError("Los campos no coinciden");
        }
    }

    //método para validar que ningún campo se encuentre vacío
    private boolean camposVacios(String nombre, String contraseña, String confirmarcontraseña, String correo, String fechanacimiento) {
        if (nombre.isEmpty()){
            edUsuarioRegistro.setError("El nombre es requerido");
            return false;
        }
        if (contraseña.isEmpty()) {
            edContrasenaRegistro.setError("La contraseña es requerida");
            return false;
        }
        if (confirmarcontraseña.isEmpty()) {
            edConfirmarContrasenaRegistro.setError("Vuelva a escribir la contraseña");
            return false;
        }
        if (correo.isEmpty()) {
            edCorreoRegistro.setError("El correo es requerido");
            return false;
        }
        if (fechanacimiento.isEmpty()) {
            Snackbar.make(layoutRegistro, "Debe indicar su fecha de nacimiento", Snackbar.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

}