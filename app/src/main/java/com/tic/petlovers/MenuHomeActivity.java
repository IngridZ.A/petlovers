package com.tic.petlovers;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.tic.petlovers.controller.PagerController;

public class MenuHomeActivity extends AppCompatActivity {
    private TabLayout tabLayoutMenuHome;
    private ViewPager viewPageMenuHome;
    private PagerController pagerController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_home);
        tabLayoutMenuHome = findViewById(R.id.tabLayoutMenuHome);
        viewPageMenuHome = findViewById(R.id.viewPageMenuHome);

        pagerController = new PagerController(getSupportFragmentManager(),tabLayoutMenuHome.getTabCount());
        viewPageMenuHome.setAdapter(pagerController);

        tabLayoutMenuHome.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {


            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPageMenuHome.setCurrentItem(tab.getPosition());
                pagerController.notifyDataSetChanged();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        viewPageMenuHome.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayoutMenuHome));

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_principal,menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item){
        switch (item.getItemId()){
            case R.id.itemCerrarSesion:
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(MenuHomeActivity.this)
                        .setTitle("Confirmar")
                        .setMessage("¿Está seguro que desea cerrar sesión?")
                        .setCancelable(false)
                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int i) {
                                Toast.makeText(MenuHomeActivity.this,"Sesión Cerrada",Toast.LENGTH_LONG).show();
                                FirebaseAuth.getInstance().signOut();
                                Intent intent = new Intent(MenuHomeActivity.this,MainActivity.class);
                                startActivity(intent);

                            }
                        })
                        .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int i) {
                                Toast.makeText(MenuHomeActivity.this,"No se ha cerrado sesión", Toast.LENGTH_LONG).show();
                            }
                        })
                        ;
                alertDialog.show();
        }
        return super.onOptionsItemSelected(item);
    }


}
