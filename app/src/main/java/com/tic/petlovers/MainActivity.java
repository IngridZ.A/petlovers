package com.tic.petlovers;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {

    private ImageView imgLogoLogin, imgNombreMain;
    private TextView txtOContrasena;
    private EditText edEmail, edPassword;
    private Button btnRegistrar, btnIngresar;
    private ToggleButton tBtnPassword;
    private FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mAuth = FirebaseAuth.getInstance();

        imgNombreMain = findViewById(R.id.imgNombreMain);
        imgLogoLogin = findViewById(R.id.imgLogoLogin);
        imgNombreMain.setImageResource(R.drawable.nombre);
        imgLogoLogin.setImageResource(R.drawable.logo);
        txtOContrasena = findViewById(R.id.txtOContrasena);
        edEmail = findViewById(R.id.edEmail);
        edPassword = findViewById(R.id.edPassword);
        tBtnPassword = findViewById(R.id.tBtnPassword);
        btnRegistrar = findViewById(R.id.btnRegistrar);
        btnIngresar = findViewById(R.id.btnIngresar);

        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                siguienteRegistrar();
            }
        });

        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        tBtnPassword.setVisibility(View.VISIBLE);
                    }
                }, 12000);

                String email = edEmail.getText().toString();
                String password = edPassword.getText().toString();

                if(!email.isEmpty()){
                    if(!password.isEmpty()){
                        mAuth.signInWithEmailAndPassword(email, password)
                                .addOnCompleteListener(MainActivity.this, new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        if (task.isSuccessful()) {
                                            Log.d("Success", "signInWithEmail:success");
                                            FirebaseUser user = mAuth.getCurrentUser();
                                            Toast.makeText(MainActivity.this, "Bienvenido "+user.getDisplayName(),
                                                    Toast.LENGTH_SHORT).show();
                                            siguienteHome();

                                        } else {
                                            Log.w("Fail", "signInWithEmail:failure", task.getException());
                                            Toast.makeText(MainActivity.this, "Autenticación Fallida",
                                                    Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                    }else{
                        tBtnPassword.setVisibility(View.INVISIBLE);
                        edPassword.setError("La contraseña es requerida");
                    }

                }else{
                    edEmail.setError("El correo es requerido");
                }

            }

        });

        //método para restablecer la contraseña de acceso
        txtOContrasena.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final EditText edCorreoAD = new EditText(MainActivity.this);
                edCorreoAD.setInputType(InputType.TYPE_CLASS_TEXT);
                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Restablezca su contraseña")
                        .setView(edCorreoAD)
                        .setMessage("Por favor ingrese el correo con el cual se registró:")
                        .setCancelable(false)
                        .setPositiveButton("Enviar", null)
                        .setNegativeButton("Cancelar", null)
                        .create();

                alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog) {
                        Button btnEnviar = alertDialog.getButton((AlertDialog.BUTTON_POSITIVE));
                        btnEnviar.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                String email = edCorreoAD.getText().toString();
                                if (!email.isEmpty()){
                                    mAuth.sendPasswordResetEmail(email).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if(task.isSuccessful()){
                                                Toast.makeText(MainActivity.this, "Revise su bandeja de entrada y siga las instrucciones",Toast.LENGTH_LONG).show();

                                            }else{
                                                Toast.makeText(MainActivity.this, "No fue posible enviar el correo",Toast.LENGTH_LONG).show();
                                                Log.w("ErrorEnvioCorreo",task.getException().toString());
                                            }
                                        }
                                    });
                                    alertDialog.dismiss();
                                }else{
                                    edCorreoAD.setError("No se ha ingresado ningún dato");
                                }

                            }
                        });
                    }
                });
                alertDialog.show();
            }
        });
    }

        //método para hacer visible la contraseña
        public void mostrarContraseña(View view){
        // Salvar cursor
        int cursor = edPassword.getSelectionEnd();

        if(tBtnPassword.isChecked()){
            edPassword.setInputType(InputType.TYPE_CLASS_TEXT
                    | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        }else{
            edPassword.setInputType(InputType.TYPE_CLASS_TEXT
                    | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        }
        edPassword.setSelection(cursor);
    }

        public void siguienteRegistrar() {
            Intent intent = new Intent(MainActivity.this,RegistroActivity.class);
            startActivity(intent);
    }

        public void siguienteHome() {
            Intent intent = new Intent(MainActivity.this,MenuHomeActivity.class);
            startActivity(intent);
            finish();
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser != null){
            siguienteHome();
        }
    }

}