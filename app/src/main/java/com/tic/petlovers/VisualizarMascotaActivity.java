package com.tic.petlovers;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.PermissionChecker;

import android.Manifest;
import android.annotation.SuppressLint;
import android.location.Location;
import android.os.Bundle;
import android.widget.ImageView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.tic.petlovers.model.Usuario;

public class VisualizarMascotaActivity extends AppCompatActivity implements OnMapReadyCallback {
    private FusedLocationProviderClient fusedLocationProviderClient;
    private ImageView imgMascotaVisualizarMascota;
    private DatabaseReference reference;
    private String nombre,raza,edad,idUsuario,telefono,direccion,urlImagen;
    private double latitud,longitud;
    private Usuario usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visualizar_mascota);
        reference = FirebaseDatabase.getInstance().getReference();
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        imgMascotaVisualizarMascota = findViewById(R.id.imgMascotaVisualizarMascota);
        nombre = getIntent().getStringExtra("nombre");
        raza = getIntent().getStringExtra("raza");
        edad = getIntent().getStringExtra("edad");
        idUsuario = getIntent().getStringExtra("idUsuario");
        telefono = getIntent().getStringExtra("telefono");
        direccion = getIntent().getStringExtra("direccion");
        latitud = getIntent().getDoubleExtra("latitud",0.0);
        longitud = getIntent().getDoubleExtra("longitud",0.0);
        Picasso.get().load("https://firebasestorage.googleapis.com/v0/b/petlovers-c7abc.appspot.com/o/Im%C3%A1genes_mascotas_en_adopci%C3%B3n%2FIMGLixIdGz6FOfu4P4IYsQh0itWzYn2-Thu%20Nov%2017%2013%3A48%3A36%20GMT-05%3A00%202022-42080.jpeg?alt=media&token=acb6cff6-1704-456c-b04c-24b0200560bf").into(imgMascotaVisualizarMascota);


        reference.child("Usuarios").child(idUsuario).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                Usuario usuario = snapshot.getValue(Usuario.class);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    private boolean permisosGps(){
        Boolean permisosAccesFine = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PermissionChecker.PERMISSION_GRANTED;
        Boolean permisosCoarseAcces = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PermissionChecker.PERMISSION_GRANTED;
        if (permisosAccesFine || permisosCoarseAcces){
            return true;

        }else{
            ActivityCompat.requestPermissions(this, new String[]{ Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},0);
            return false;
        }

    }
    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        LatLng misCoordenadas = new LatLng(latitud,longitud);
        if (permisosGps()){
            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
            fusedLocationProviderClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    if (location!= null){
                        latitud = location.getLatitude();
                        longitud = location.getLongitude();

                        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(misCoordenadas,14));//Envía las coordenadas y el acercamiento de la cámara
                        googleMap.addMarker(new MarkerOptions().position(misCoordenadas).title("Mi mascota"));
                        UiSettings settings = googleMap.getUiSettings();
                        settings.setZoomControlsEnabled(true); //Esta línea agrega el zoom en el mapa
                        settings.setCompassEnabled(true);//Agrega la brújula en el mapa
                    }
                }
            });
        }
    }
}