package com.tic.petlovers.model;


public class Usuario {
    private String nombre, correo, fechaDeNacimiento;

    public Usuario(String nombre, String correo, String fechaDeNacimiento) {
        this.nombre = nombre;
        this.correo = correo;
        this.fechaDeNacimiento = fechaDeNacimiento;
    }
    public Usuario(){

    }

    @Override
    public String toString() {
        return "Usuario{" +
                "nombre='" + nombre + '\'' +
                ", correo='" + correo + '\'' +
                ", fechaDeNacimiento='" + fechaDeNacimiento + '\'' +
                '}';
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getFechaDeNacimiento() {
        return fechaDeNacimiento;
    }

    public void setFechaDeNacimiento(String fechaDeNacimiento) {
        this.fechaDeNacimiento = fechaDeNacimiento;
    }
}

