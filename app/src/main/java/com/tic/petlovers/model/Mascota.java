package com.tic.petlovers.model;

import com.google.firebase.database.IgnoreExtraProperties;

import java.util.ArrayList;

@IgnoreExtraProperties
public class Mascota {
    private String categoria, genero, nombre, raza, telefono, direccion, edad, urlImagen;
    private Integer idImagen; //Temporal
    private String idUsuario; //Obtener el usuario que realiza el registro desde la BD
    private Boolean favorito;
    private ArrayList<String> vacunas;
    private Double longitud, latitud;


    //clase utilizada para subir información a la BD
    public Mascota(String nombre, String raza, String telefono, String direccion, String edad, String urlImagen, String idUsuario, ArrayList<String> vacunas, Double longitud, Double latitud) {
        this.nombre = nombre;
        this.raza = raza;
        this.telefono = telefono;
        this.direccion = direccion;
        this.edad = edad;
        this.urlImagen = urlImagen;
        this.idUsuario = idUsuario;
        this.vacunas = vacunas;
        this.longitud = longitud;
        this.latitud = latitud;


    }

    public Mascota() {
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public String getUrlImagen() {
        return urlImagen;
    }

    public void setUrlImagen(String urlImagen) {
        this.urlImagen = urlImagen;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Boolean isFavorito() {
        return favorito;
    }

    public void setFavorito(Boolean favorito) {
        this.favorito = favorito;
    }

    public ArrayList<String> getVacunas() {
        return vacunas;
    }

    public void setVacunas(ArrayList<String> vacunas) {
        this.vacunas = vacunas;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }

    public Double getLatitud() {
        return latitud;
    }

    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    public Integer getIdImagen() {
        return idImagen;
    }

    public void setIdImagen(Integer idImagen) {
        this.idImagen = idImagen;
    }
}
